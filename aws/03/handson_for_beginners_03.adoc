= Hands-on for Beginners サーバレスアーキテクチャによるWeb APIの構築

image::agenda.png[]
image::overview.png[]

== Step 01 AWS Lambdaを単体で使ってみる

image::step01.png[]

.AWS Lambda
|===
| 設定項目| 設定値1 | 設定値2

| オプション | 一から作成 | -
| 関数名  | translate-function | -
| ランタイム  | Python 3.9 | -
| ロール  | 基本的なLambdaアクセス権限で新しいロールを作成 | -
|===

[source, python]
----
import json
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event, context):

    logger.info(event)

    return {
        'statusCode': 200,
        'body': json.dumps('Hello Hands on world!')
    }
----

* IAMロールの内容確認
** IAMロールに設定されたポリシーの内容を表示
* パラメータの変更
** メモリ（MB）を256MBに変更
** タイムアウトを10秒に変更
** 変更内容を保存
* テスト実行
** テストイベントの設定（イベントの内容はデフォルトのまま）
** テスト実行
* ログ出力の実装
** ログ出力の実装
** Lambdaのコンソールからログ出力結果を確認
** CloudWatchを参照してログ出力結果を確認

== Step 02 AWS Lambdaで他のサービスを呼び出してみる

image::step02.png[]

[source, python]
----
import json
import boto3

translate = boto3.client('translate')

def lambda_handler(event, context):

    input_text = "おはよう"

    response = translate.translate_text(
        Text=input_text,
        SourceLanguageCode='ja',
        TargetLanguageCode='en'
    )

    output_text = response.get('TranslatedText')

    return {
        'statusCode': 200,
        'body': json.dumps({
            'output_text': output_text
        })
    }
----

* IAMロールの修正
** IAMのコンソールからTranslateFullAccessポリシーを追加
* テスト実行

== Step 03 Amazon API Gatewayを単体で使ってみる

image::step03.png[]

* Amazon API Gatewayの作成

.API Gateway
|===
| 設定項目| 設定値1 | 設定値2

| APIタイプ | REST API | -
| プロトコル | REST | -
| 新しいAPIの作成  | 新しいAPI | -
| API名  | translate-api | -
| エンドポイントタイプ  | リージョン | -
|===

* sampleリソースの作成

.API Gateway sampleリソース
|===
| 設定項目| 設定値1 | 設定値2

| リソース名 | sample | -
|===

* GETメソッドの作成

.API Gateway GETメソッド
|===
| 設定項目| 設定値1 | 設定値2

| 統合タイプ | Mock | -
|===

* レスポンスデータの作成
** 統合レスポンスのデフォルトのメソッドレスポンスを選択
** マッピングテンプレートのapplication/jsonを選択し、内容を編集

[source, json]
----
{
    "statusCode": 200,
    "body": {
        {
            "report_id": 5,
            "report_title" : "Hello, world"
        },
        {
            "report_id": 7,
            "report_title" : "Good morning!"
        }
    }
}
----

* テストの実行
* APIのデプロイ
** 新しいステージとして「dev」ステージを作成

== Step 04 API GatewayとLambdaを組み合わせる

image::step04.png[]


* sampleリソースの削除
* translateリソースの作成

.API Gateway sampleリソース
|===
| 設定項目| 設定値1 | 設定値2

| リソース名 | translate | -
|===

* GETメソッドの作成

.API Gateway GETメソッド
|===
| 設定項目| 設定値1 | 設定値2

| 統合タイプ | Lambda関数 | -
| Lambdaプロキシ統合の使用 | 使用する | -
| Lambda関数 | translate-function | -
|===

* クエリパラメータの追加
** メソッドリクエストを選択し、URLクエリ文字列パラメータとして「input_text」を作成し、必須にチェックを入れる

* Lambda関数の修正
** テストイベントの新規作成から「Amazon API Gateway AWS Proxy」を選択し、queryStringPrametersに「input_text」を追加して作成
** ソースコードの修正

[source, python]
----
import json
import boto3

translate = boto3.client(service_name='translate')

def lambda_handler(event, context):

    # クエリパラメータの取得
    input_text = event['queryStringParameters']['input_text']

    response = translate.translate_text(
        Text=input_text,
        SourceLanguageCode="ja",
        TargetLanguageCode="en"
    )

    output_text = response.get('TranslatedText')

    # API Gatewayの仕様に合わせたレスポンス形式に変更
    return {
        'statusCode': 200,
        'body': json.dumps({
            'output_text': output_text
        }),
        'isBase64Encoded': False,
        'headers': {}
    }
----

* API Gatewayのデプロイ
** devステージにデプロイ
** ブラウザから動作確認

== Step 05 DynamoDBにテーブルを作ってみる

image::step05.png[]

* DynamoDBテーブルの作成

.DynamoDB
|===
| 設定項目| 設定値1 | 設定値2

| テーブル名 | translate-history | -
| プライマリーキー | timestamp | 文字列
| 設定 | 設定のカスタマイズ | -
| キャパシティーモード | プロビジョンド | -
| 読み込みキャパシティー - AutoScaling | オフ | -
| 読み込みキャパシティー - プロビジョンドキャパシティーユニット | 1 | -
| 書き込みキャパシティー - AutoScaling | オフ | -
| 書き込みキャパシティー - プロビジョンドキャパシティーユニット | 1 | -

|===

* アイテムの手動追加
** 項目タブを選択し、項目の作成

.DynamoDBアイテムの作成
|===
| 設定項目| 設定値1 | 設定値2

| timestamp | String | 20210901100000
| input_text | String | ★こんにちは
| output_text | String | ★Hello
|===


== Step 06 API GatewayとLambdaとDynamoDBを組み合わせる

image::step06.png[]

* Lambda関数の修正

[source, python]
----
import json
import boto3
import datetime

translate = boto3.client(service_name='translate')

dynamodb_translate_history_tbl = boto3.resource('dynamodb').Table('translate-history')

def lambda_handler(event, context):

    input_text = event['queryStringParameters']['input_text']

    response = translate.translate_text(
        Text=input_text,
        SourceLanguageCode="ja",
        TargetLanguageCode="en"
    )

    output_text = response.get('TranslatedText')

    dynamodb_translate_history_tbl.put_item(
      Item = {
        "timestamp": datetime.datetime.now().strftime("%Y%m%d%H%M%S"),
        "input_text": input_text,
        "output_text": output_text
      }
    )

    return {
        'statusCode': 200,
        'body': json.dumps({
            'output_text': output_text
        }),
        'isBase64Encoded': False,
        'headers': {}
    }
----

* IAMロールの修正
** IAMのコンソールからAmazonDynamoDBFullAccessポリシーを追加
* テスト実行
* DynamoDBの内容確認
